import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';

// Define your GraphQL endpoint URL
const GRAPHQL_URL = 'https://api.github.com/graphql';

// Set up a link to the GraphQL endpoint with your Bearer token
const httpLink = createHttpLink({
    uri: GRAPHQL_URL,
    headers: {
        Authorization: `Bearer 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`, // Replace with your actual token
    },
});

// Create an Apollo Client instance
const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
});

export default client;