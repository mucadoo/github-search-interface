import {createTheme} from "@mui/material/styles";

const theme = createTheme({
    palette: {
        primary: {
            main: '#005161',
        },
        secondary: {
            main: '#30C7AD',
        },
    },
});

export default theme;